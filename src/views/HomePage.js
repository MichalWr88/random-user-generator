import React from "react";
import PropTypes from "prop-types";
import MainTemplate from "templates/MainTemplate";
import Row from "react-bootstrap/Row";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import styled from 'styled-components';
import bg from 'assets/athletes-ball.jpg';

const Wrapper = styled.div`
height:100%;
display:flex;
justify-content:center;
align-items:flex-end;
padding:20px;
`


const HomePage = props => {
  return (
    <MainTemplate >
<Container as={Wrapper}>

        <Card>
          <Card.Header as="h5">React Random User Generator</Card.Header >
          {/* <Card.Img variant="top" src={bg} as={Bg} /> */}
          <Card.Body >
            <Card.Title>Special title treatment</Card.Title>
            <Card.Text>
Żadne dane nie są przechowywane przez aplikacje , uwierzytelnienie ma na celu dydaktyk
            </Card.Text>
            <Button variant="primary">Loginwith google</Button>
          </Card.Body>
        </Card>
</Container>

    </MainTemplate>
  );
};

HomePage.propTypes = {};

export default HomePage;
