import React, { useState } from 'react';
import "./FilterList.css";
export default function FilterList(props) {
	const genderLsit = ['all', 'female', 'male'],
		nationalitiesList = [
			'AU',
			'BR',
			'CA',
			'CH',
			'DE',
			'DK',
			'ES',
			'FI',
			'FR',
			'GB',
			'IE',
			'IR',
			'NO',
			'NL',
			'NZ',
			'TR',
			'US',
		];

	const [gender, setgender] = useState('all');
	const checked = e => {
		console.log(e);
	};
	const setKey = (e, index) => {
		if (typeof window.btoa !== 'function') return index;
		return window.btoa(e + index);
	};
	return (
		<div>
			<form>
				<div className='form-group col-6 custom-control custom-radio custom-control-inline'>
					{genderLsit.map((item, index) => { 
						return (
							<div
								className=' custom-control custom-radio'
								key={setKey(item, index)}>
								<input
									className='custom-control-input'
									name='gender'
									type='radio'
									id={`gender${index}`}
								/>
								<label
									className='custom-control-label'
									htmlFor={`gender${index}`}
									onClick={checked}>
									{item}
								</label>
							</div>
						);
					})}
				</div>
				<div className='form-group col-6 form-inline'>
					{nationalitiesList.map((item, index) => {
						return (
							<div className='custom-control custom-checkbox m-2 p-0 border own__wrapper' key={setKey(item, index)}>
								<input
									type='checkbox'
									className='custom-control-input '
									id={`nat${index}`}
								/>
								<label
									className='custom-control-label own__checkbox p-3'
									htmlFor={`nat${index}`}>
									{item}
								</label>
							</div>
						);
					})}
				</div>
				<div className='form-group'>
					<label htmlFor='exampleInputPassword1'>Password</label>
					<input
						type='password'
						className='form-control'
						id='exampleInputPassword1'
						placeholder='Password'
					/>
				</div>
				<div className='form-group form-check'>
					<input
						type='checkbox'
						className='form-check-input '
						id='exampleCheck1'
					/>
					<label className='form-check-label' htmlFor='exampleCheck1'>
						Check me out
					</label>
				</div>
				<button type='submit' className='btn btn-primary'>
					Submit
				</button>
			</form>
		</div>
	);
}
