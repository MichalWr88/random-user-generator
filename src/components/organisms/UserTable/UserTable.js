import React, { Component } from 'react';
import ReactTable from 'react-table';
import UserProvider from '../../../api/UserProvider';
import { selectUser } from "../../../actions/index";
import { connect } from 'react-redux';

import './node_modules/react-table/react-table.css';
import './UserTable.css';

class UserTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: undefined,
			page: 0,
			selected: null,
			loading: false,
			pageSize: 5,
		};
	}
	get columns() {
		return this.props.columns
			? this.props.columns
			: [
				{
					Header: 'IMG',
					accessor: 'picture.thumbnail',
					Cell: props => (
						<figure className='figure'>
							<img
								src={props.value}
								className='figure-img img-fluid rounded'
								alt='...'
							/>
							<figcaption className='figure-caption' />
						</figure>
					),
				},
				{
					Header: 'Gender',
					accessor: 'gender',
				},
				{
					id: 'nameFirst',
					Header: 'Name',
					accessor: d => d.name.first,
				},
				{
					id: 'nameLast',
					Header: 'Surname',
					accessor: d => d.name.last,
				},
				{
					id: 'dob',
					Header: 'Age',
					accessor: d => d.dob.age,
				},
				{
					Header: 'Name',
					id: 'edit',
					Cell: ({ row }) => {
						return (
							<button
								className='btn btn-info'
								onClick={e => this.clickRow(row)}>
								details
								</button>
						);
					},
				},
			];
	}
	clickRow(e) {
		const user = e._original;
		this.props.selectUser(user)
	}
	LoadingComponent = () => {
		return this.state.loading ? (
			<div
				className='position-absolute spinner-border text-primary'
				role='status'>
				<span className='sr-only'>Loading...</span>
			</div>) : null;
	};


	gridTableprovider = (state, instance) => {
		const { page, pageSize } = state;
		if (page === this.state.page && pageSize === this.state.pageSize && this.state.data !== undefined) return;

		this.setState(() => { return { loading: true }; });

		UserProvider.getUsersList(state).then(resp => {
			console.log(resp);
			this.setState(() => {
				return {
					data: resp.results,
					page: state.page,
					loading: false,
				};
			});
		});
	};

	render() {
		const {
			pageSize,
			LoadingComponent,
			columns,
			gridTableprovider,
			state: { data },
			state: { page },
			state: { loading },
		} = this;

		return (
			<div>
				<div className='spiner__wrapper'>test</div>
				<ReactTable
					loading={loading}
					data={data}
					columns={columns}
					defaultPageSize={5}
					page={page}
					pageSize={pageSize}
					onFetchData={gridTableprovider}
				/>
			</div>
		);
	}
}

const mapStateToProps = state => {
	console.log(state);
	return { selectUser: state.selectUser }
}
export default connect(mapStateToProps, { selectUser })(UserTable)