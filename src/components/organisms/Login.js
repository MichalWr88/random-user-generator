import React, { useEffect, useState, Fragment } from 'react'
import { signIn, signOut } from "../../actions/index";
import { connect } from 'react-redux';

let auth = null;
const Login = (props) => {
	// const [auth, setAuth] = useState(null);
	// const [isSignIn, setIsSignIn] = useState(null);

	useEffect(() => {
		window.gapi.load('client:auth2', () => {
			window.gapi.client.init({
				clientId: '122653303110-dp8q3ls2gp5g9d5errmi9om047jrih3o.apps.googleusercontent.com',
				scope: 'profile'
			}).then(() => {
				// setAuth(window.gapi.auth2.getAuthInstance());
				auth = window.gapi.auth2.getAuthInstance();
				onAuthChange(auth.isSignedIn.get())
				auth.isSignedIn.listen(onAuthChange);
			})
		})
	}, [])
	const onAuthChange = (isSignedIn) => {
		if (isSignedIn) {
			props.signIn()
		} else {
			props.signOut()
		}
	}
	const signIn = () => {
		auth.signIn().then((resp) => {
			console.log(resp);

		})
		console.log(auth.currentUser.get().getBasicProfile());
		
	}
	const signOut = () => {
		auth.signOut().then((resp) => {
			console.log(resp);

		})

	}
	const GenerateBtnLogIn = () => {
		if (props.isSignedIn === null) {
			return (<div className="spinner-border" role="status">
				<span className="sr-only">Loading...</span>
			</div>)
		} else if (props.isSignedIn === true) {
			return <div className="alert alert-primary" role="alert">
				You are LogIn
				<button type="button" className="m-2 btn btn-primary" onClick={signOut}>Log out</button>
			</div>
		} else {
			return (<button type="button" className="btn btn-primary" onClick={signIn}>Log in</button>)
		}

	}

	return (
		<div className="container">
			<form className="px-4 py-3" >
				<div className="form-group">
					<label htmlFor="exampleDropdownFormEmail1">Email address</label>
					<input type="email" className="form-control" id="exampleDropdownFormEmail1" placeholder="email@example.com" />
				</div>
				<div className="form-group">
					<label htmlFor="exampleDropdownFormPassword1">Password</label>
					<input type="password" className="form-control" id="exampleDropdownFormPassword1" placeholder="Password" />
				</div>
				<div className="form-group">
					<div className="form-check">
						<input type="checkbox" className="form-check-input" id="dropdownCheck" />
						<label className="form-check-label" htmlFor="dropdownCheck">
							Remember me
        </label>
					</div>
				</div>
				{ GenerateBtnLogIn() }

			</form>
			<div className="dropdown-divider"></div>
		</div>
	)
}
const mapStateToProps = (state)=>{
	return {isSignedIn:state.auth.isSignedIn} 
}
export default connect(mapStateToProps, { signIn, signOut })(Login)
