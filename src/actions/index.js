import { SIGN_IN, SIGN_OUT, SELECTED_USERS } from "./types";

export const getUsers = (users) => {
	return {
		type: "GET_USERS",
		payload: [...users]
	}
}
export const selectUser = (user) => {
	return {
		type: SELECTED_USERS,
		payload: user
	}
}
export const signIn = () => {
	return {
		type: SIGN_IN
	}
}
export const signOut = () => {
	return {
		type: SIGN_OUT
	}
}

