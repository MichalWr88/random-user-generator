import React, { Component } from 'react'

export default class NotFound extends Component {
	render() {
		return (
			<div className="container-fluid ">
				<div className="container d-flex justify-content-center align-items-center border">
					<p className="p-2">4</p>
					<img src={process.env.PUBLIC_URL + '/images/astronaut.svg'} alt="astronaut" />
					<p className="p-2">4</p>
				</div>
				<p>
					You are out of space app
				</p>
				<p>
					Great shot, kid. That was one in a milion.
				</p>
				<p>
					Let's get back
				</p>

			</div>
		)
	}
}
