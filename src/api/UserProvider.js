const apiUrl = 'https://randomuser.me/api/';

const UserProvider = {
	getUser: () => {
		return fetch(`${apiUrl}`).then(response => response.json());
	},
	getUsersList(config) {
		const { page } = config
		return fetch(`${apiUrl}?results=${100}&page=${page}&seed=abc`).then(response => response.json());
	},
};
export default UserProvider;
