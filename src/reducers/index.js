
import { combineReducers } from "redux";
import authReducer from "./authReducer";

const usersReducer = (users = []) => {
	return [...users]
}

const selectedUserReducer = (selected = null, action) => {
	if (action.type === "SELECTED_USERS") {
		return action.payload;
	}
	return selected;
}

export default combineReducers({
	users: usersReducer,
	selecteduser: selectedUserReducer,
	auth: authReducer
})