import React from 'react';
import { BrowserRouter as Router, Route,  Switch } from "react-router-dom";
import routes from 'routes'
import 'bootstrap/dist/css/bootstrap.min.css'




const App = () => {
	return (
		<Router>
				<Switch>
					{routes.map((route, index) => {
						return (<Route
							key={index}
							path={route.path}
							exact={route.exact}
							component={route.component}
						/>)
					})}

				</Switch>
		</Router>
	);
}

export default App;
