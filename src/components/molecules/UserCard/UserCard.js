import React, { PureComponent } from 'react';
import UserProvider from '../../../api/UserProvider';
//import { Test } from './UserCard.styles';

class UserCard extends PureComponent {
	constructor(props) {
		super(props);

		this.state = {
			hasError: false,
			data: undefined,
		};
	}

	componentWillMount = () => {
		console.log('UserCard will mount');
	};

	componentDidMount = () => {
		UserProvider.getUser().then(resp => {
			console.log(resp);
			this.setState(() => {
				return { data: resp.results[0] };
			});
		});
		// UserProvider.getUsersList().then(resp => {
		// 	console.log(resp);
		// 	// this.setState(() => {
		// 	// 	return { data: resp.results[0] };
		// 	// });
		// });
		console.log('UserCard mounted');
	};

	componentWillReceiveProps = nextProps => {
		console.log('UserCard will receive props', nextProps);
	};

	componentWillUpdate = (nextProps, nextState) => {
		console.log('UserCard will update', nextProps, nextState);
	};

	componentDidUpdate = () => {
		console.log('UserCard did update');
	};

	componentWillUnmount = () => {
		console.log('UserCard will unmount');
	};

	render() {
		if (this.state.hasError) {
			return <h1>Something went wrong.</h1>;
		}
		return (
			(this.state.data !== undefined)?
			<div className='card mb-3' style={{ maxWidth: '540px' }}>
				<div className='row no-gutters'>
					<div className='col-md-4'>
						{/* <img
								src={this.state.data.picture.large}
							className='card-img'
							alt='...'
						/> */}
						
					</div>
					<div className='col-md-8'>
						<div className='card-body'>
								<h5 className='card-title text-uppercase bg-info text-light p-2'>{this.state.data.name.first} {this.state.data.name.last}</h5>
							<p className='card-text'>
								This is a wider card with supporting text below
								as a natural lead-in to additional content. This
								content is a little bit longer.
							</p>
							<p className='card-text'>
								<small className='text-muted'>
									Last updated 3 mins ago
								</small>
							</p>
						</div>
					</div>
				</div>
			</div>:null
		);
	}
}



UserCard.defaultProps = {
	// bla: 'test',
};

export default UserCard;
