import React from 'react'
import Container from 'react-bootstrap/Container'
import styled from 'styled-components'
import bg from 'assets/athletes-ball.jpg';

const Wrapper = styled.div`
height:100vh;
overflow:auto;
position:relative;
background-image: url(${bg});
background-size: cover;
// background-repeat: no-repeat;
background-position: center;
`

const MainTemplate = (props) => {
    const {children} = props;
    return (
        <Container fluid as={Wrapper} >
            {children}
        </Container>
    )
}

export default MainTemplate
