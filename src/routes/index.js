import Home from "views//HomePage";
// import  Login  from "../components/organisms/Login";

const routes = [
	{
		path: "/",
		exact: true,
		protected: false,
		sidebar: true,
		component: Home
	},
	{
		path: "/users",
		protected: true,
		sidebar: true,
		component: "UserTable"
	},
	{
		path: "/details",
		protected: true,
		sidebar: true,
		component: "UserCard"
	},
	// {
	// 	path: "/login",
	// 	protected: false,
	// 	sidebar: false,
	// 	component: Login
	// },
	{
		path: "/notFound",
		protected: false,
		sidebar: false,
		component: "NotFound"
	}
];
export default routes